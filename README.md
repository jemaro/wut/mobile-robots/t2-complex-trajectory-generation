# T2 Complex trajectory generation

Please, consider [reading this report
online](https://gitlab.com/jemaro/wut/mobile-robots/t2-complex-trajectory-generation/-/blob/master/README.md)
in order to be able to see the results animations.

### [Andreu Gimenez Bolinches](01163563@pw.edu.pl), student number: 317486

The main goal of the first tutorial is to learn to control a robot using
multiple behaviours governed by a finite state machine (FSM). 

## Solution
Comments can be find in the code that explain the solution step by step
```matlab
function [forwBackVel, leftRightVel, rotVel, finish] = solution2(...
        pts, contacts, position, orientation, varargin ...
        )
    % Initialize constants
    MAX_VEL = 5;
    KP_R = 1;
    KP_ROT_VEL = 10; MAX_ROT_VEL = 10;
    finish = false; % The robot can move indefinitelly

    % Get input parameters
    switch length(varargin)
        case 3
            [d, l, a] = deal(varargin{:});
            goal_att = 0;
        case 4
            [d, l, a, goal_att] = deal(varargin{:});
        case 5
            [d, l, a, goal_att, MAX_VEL] = deal(varargin{:});
        otherwise
            error('At least three trajectory arguments must be provided: diameter, length, angle');
    end

    % Check inputs
    if d > l
        error('Trajectory diameter cannot be greater than the length')
    end

    a = wrapToPi(a);
    goal_att = wrapToPi(goal_att);

    % Extract interesting robot parameters
    pos = position(1:2);
    att = orientation(3);

    % Finite State Machine variables
    [STRAIGHT, HALF_CIRCLE] = assign_states();
    persistent state;
    persistent first_turn goal_pos goal_pos_1 center;
    % first_turn (bool): whether is the first half of the trajectory or not
    % goal_pos (1x2 int): position of the current goal
    % goal_pos_1 (1x2 int): position of the previous goal
    % center (1x2 int): position of the center of rotation when turning

    % Initialize Finite State Machine
    if isempty(state)
        first_turn = 1;
        state = STRAIGHT;
        goal_pos_1 = compute_circle_goal(l, d, a, 0);
        goal_pos = compute_straight_goal(l, d, a, first_turn);
        disp('Start moving now')
    end

    %% Compute state changes
    % Check first if the goal has been reached
    switch state
        case STRAIGHT
            % Check that we have traveled the required length from the previous
            % goal instead of checking if we are close enough to the current
            % goal. This way, even if the actual trajectory has been deformed
            % because of wheel slipping, we will start the turn and correct the
            % discrepancies during the turn. It allows the robot to never slow
            % down while moving
            goal_err = l - d - norm(pos - goal_pos_1);

            if goal_err < 0
                state = HALF_CIRCLE;
                % Compute next goals and turn center
                center = compute_center(l, d, a, first_turn);
                goal_pos_1 = goal_pos;
                goal_pos = compute_circle_goal(l, d, a, first_turn);
                disp('Goal reached, turn')
            end

        case HALF_CIRCLE
            % Check if the robot has turned 180 degrees from the previous goal
            n2 = pos - center;
            n1 = goal_pos_1 - center;
            traveled = atan2(dot(n1, n2 * [0 1; -1 0]), dot(n1, n2));

            if traveled < 0 % When the angle is greater than 180ª it is negative
                state = STRAIGHT;
                % Specify that we are in the next half of our trajectory
                first_turn = (1 - first_turn);
                % Compute next goals
                goal_pos_1 = goal_pos;
                goal_pos = compute_straight_goal(l, d, a, first_turn);
                disp('Turn completed, move straight')
            end

        otherwise
            error('Unknown state %s.\n', state);
    end

    %% Control position depending on the finite state machine
    % The finite state machine will define a direction for the movement, which
    % then will be followed at a constant velocity. This solution can be
    % expanded with more complex velocity controls.
    switch state
        case STRAIGHT
            % If velocity where to be increased significantly, the direction
            % control of the straight path could be improved by modelling it as
            % an infinite radius circle. This way, the robot would attempt to
            % actually follow the original straight line instead of just trying
            % to reach the goal as fast as possible
            diff_pos = goal_pos - pos;
            direction = diff_pos / norm(diff_pos);
        case HALF_CIRCLE
            % Radius error
            center_pos = center - pos;
            center_dist = norm(center_pos);
            radius_err = center_dist - d / 2;
            % Compute the weight of the radial direction in the movement
            P = radius_err * KP_R;
            r = max(min(P, 1), -1);
            % Compute the movement direction
            r_dir = center_pos / center_dist;
            t_dir = tangential_direction(r_dir);
            direction = r * r_dir + (1 - r) * t_dir;
        otherwise
            error('Unknown state %s.\n', state);
    end

    %% Compute global velocity
    % As we do not wish the robot to stop at any point, the speed will always
    % be the maximum speed in the direction that the control algorithm
    % computed.
    vel_G = MAX_VEL * direction;
    %% Compute the local velocity from the global
    % By making use of the coordinate system transformation, we convert our
    % calculated global velocity into local values to feed to the motor
    % controllers
    vel_L = global2local(vel_G, att);
    leftRightVel = vel_L(1);
    forwBackVel = vel_L(2);

    %% Control orientation
    % Difference in attitude
    err_att = angdiff(goal_att, att);
    % Compute the necessary rotational velocity
    P = err_att * KP_ROT_VEL;
    rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);

end

function goal = compute_straight_goal(l, d, a, first_turn)
    goal = compute_goal(l, d, a, first_turn, 1);
end

function goal = compute_circle_goal(l, d, a, first_turn)
    goal = compute_goal(l, d, a, first_turn, 0);
end

function goal = compute_goal(l, d, a, first_turn, straight)
    % Without angle adjust it would look like this
    % if first_turn == 1 && straight == 1
    %   goal = [-(l-d)/2 0];
    % if first_turn == 0 && straight == 1
    %   goal = [(l-d)/2 d];
    % if first_turn == 1 && straight == 0
    %   goal = [-(l-d)/2 d];
    % if first_turn == 0 && straight == 0
    %   goal = [(l-d)/2 0];
    goal = [(1 - 2 * first_turn) * (l - d) / 2, abs(straight - first_turn) * d];
    goal = correct_angle(goal, a);
end

function goal = compute_center(l, d, a, first_turn)
    % Without angle adjust it would look like this
    % if first_turn == 1
    %   goal = [-(l-d)/2 d/2];
    % if first_turn == 0
    %   goal = [(l-d)/2 d/2];
    goal = correct_angle([(1 - 2 * first_turn) * (l - d) / 2 d / 2], a);
end

function t_dir = tangential_direction(r_dir)
    % Always clockwise
    t_dir = r_dir * [0 1; -1 0];
end

function point = correct_angle(point, a)
    % Applies the rotation angle to a trajectory point
    point = point * [cos(a) -sin(a); sin(a) cos(a)];
end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end

function vel_L = global2local(vel_G, theta)
    % vel_G: Desired velocity expressed in the global frame
    % theta: Get current rotation of the robot

    % Calculate rotation matrix - transformation from the global frame
    % to the local frame of the robot
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];

    % Desired velocity expressed in the local frame
    vel_L = vel_G * R;
end
```

## Results

Three arguments are required, diameter, length and angle of the trajectory. Additionally, the robot orientation argument can be specified as the forth argument:
```
run_simulation(@solution2, false, 1, 3, 0.5, pi)
```
![solution2_1](solution2_1.png)
![solution2_1](solution2_1.gif)

Another example with a trajectory closer to a circle:
```
run_simulation(@solution2, false, 3, 4, pi/2)
```
![solution2_2](solution2_2.png)
![solution2_2](solution2_2.gif)

One can make use of a fifth argument to override the maximum velocity. We can
test in this way that the robot control does not break when attempting
unreasonable speeds that make the wheels slide. Nevertheless the trajectory is
not followed as closely due to that same fact.
```
run_simulation(@solution2, false, 3, 4, pi/2, pi/2, 30)
```
![solution2_3](solution2_3.png)